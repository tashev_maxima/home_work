﻿namespace HW_14_ExtesionBubbleSort;

public static class BubbleSort
{
    
    
   public static void Sort(this int[] array)
    {
        var rand = new Random();
        for (int i = 0; i < array.Length; i++)
        {
            array[i] = rand.Next(-10, 10);
        }
        Console.WriteLine($"Рандомный массив:\n");
        for (int i = 0; i < array.Length; i++)
        {
            Console.WriteLine(array[i]);
        }
        
        int temp;
        for (int i = 0; i < array.Length; i++)
        {
            for (int j = i + 1; j < array.Length; j++)
            {
                if (array[i] > array[j])
                {
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }                   
            }            
        }
        {
            Console.WriteLine($"Отсортированный массив:\n");
        }
        for (int i = 0; i < array.Length; i++)
        {
            Console.WriteLine(array[i]);
        }
        

    }
   
}
