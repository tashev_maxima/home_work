﻿namespace HW_15_Library;

//реализовать в проекте обобщенную структуру данных стек (на основе массива или листа)(push, pop, peek)
public class CustomStack<T>
{
    
    private readonly List<T> _list;

    public CustomStack(int capacity)
    {
        _list = new List<T>(capacity);
    }
    
    public int Capacity
    {
        get { return _list.Capacity; }
    }

   
    public void Push(T item)
    {
       _list.Add(item);
    }


    public T? Pop()
    {
       
        var item = _list.LastOrDefault();
        _list.RemoveAt(_list.Count - 1);
        return item;
    }


    public T? Peek()
    {
        var item = _list.FirstOrDefault();
        return item;
    }

    public void Print() // Выводит все элементы в стеке
    {
        Console.WriteLine($"Список элементов в стеке:");
        for (int i = 0; i < _list.Count; i++)
        {
            Console.WriteLine(_list[i]);
        }
    }
      
}