﻿namespace HW_17._1;

public class Entrant
{
    public string LastName { get; set; }
    public int YearOfAdmission { get; set; }
    public int NumberOfSchool { get; set; }
    
}