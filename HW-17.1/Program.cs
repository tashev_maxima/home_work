﻿/*Дано целое число K (> 0) и целочисленная последовательность A. Найти теоретико-множественную 
разность двух фрагментов A: первый содержит все четные числа, а второй — все числа с порядковыми 
    номерами, большими K. В полученной последовательности (не содержащей одинаковых элементов) 
поменять порядок элементов на обратный. 
    ● Даны целые числа K1 и K2 и целочисленные последовательности A и B. Получить последовательность, 
    содержащую все числа из A, большие K1, и все числа из B, меньшие K2. Отсортировать полученную 
последовательность по возрастанию. 
    ● Исходная последовательность содержит сведения об абитуриентах. Каждый элемент 
последовательности включает следующие поля: 
    <Фамилия> <Год поступления> <Номер школы> 
    Для каждой школы вывести общее число абитуриентов за все годы и фамилию первого из абитуриентов 
этой школы, содержащихся в исходном наборе данных (вначале указывать номер школы, затем число 
    абитуриентов, затем фамилию). Сведения о каждой школе выводить на новой строке и упорядочивать 
по возрастанию номеров школ.*/

using HW_17._1;

int K = 12;

var A = new List<int>();

for (int i = 0; i < 10; i++)
{
    var rndNum = new Random().Next(1, 20);
    A.Add(rndNum);
}
Console.ForegroundColor = ConsoleColor.Red;
Console.WriteLine("ервая часть часть");
Console.ForegroundColor = ConsoleColor.White;
Console.WriteLine("Целочисленная последовательность A:");
Console.ForegroundColor = ConsoleColor.White;

foreach (var num in A)
{
    Console.Write($"{num}\t");
}

var result = A
    .Where(x => x % 2 == 0)
    .Except(A.Where(x => x > K))
    .Distinct()
    .Reverse();

Console.WriteLine("\nТеоретико-множественную разность двух фрагментов A:");
foreach (var num in result)
{
    Console.Write($"{num}\t");
}

int K1 = 12;
int K2 = 10;

var _A = new List<int>();

for (int i = 0; i < 10; i++)
{
    var rndNum = new Random().Next(1, 20);
    _A.Add(rndNum);
}
Console.ForegroundColor = ConsoleColor.Red;
Console.WriteLine("\nВторая часть");
Console.ForegroundColor = ConsoleColor.White;
Console.WriteLine("Целочисленные последовательности A:");
foreach (var num in _A)
{
    Console.Write($"{num}\t");
}

var _B = new List<int>();

for (int i = 0; i < 10; i++)
{
    var rndNum = new Random().Next(1, 20);
    _B.Add(rndNum);
}

Console.Write("\nЦелочисленные последовательности B:\n");

foreach (var num in _B)
{
    Console.Write($"{num}\t");
}

var AB = _A
    .Where(x => x > K1)
    .Union(_B.Where(x => x < K2))
    .OrderBy(x => x)
    .ToList();

Console.Write("\nИтог:\n");

foreach (var num in AB)
{
    Console.Write($"{num}\t");
}

Console.ForegroundColor = ConsoleColor.Red;
Console.Write("\nТретья часть\n");
Console.ForegroundColor = ConsoleColor.White;

var entrants = new List<Entrant>();

entrants.Add(new Entrant() {LastName = "Гончаров", YearOfAdmission = 2022, NumberOfSchool = 20});
entrants.Add(new Entrant() {LastName = "Алексеев", YearOfAdmission = 2022, NumberOfSchool = 20});
entrants.Add(new Entrant() {LastName = "Иванов", YearOfAdmission = 2021, NumberOfSchool = 62});
entrants.Add(new Entrant() {LastName = "Петров", YearOfAdmission = 2021, NumberOfSchool = 62});
entrants.Add(new Entrant() {LastName = "Зябкин", YearOfAdmission = 2020, NumberOfSchool = 10});
entrants.Add(new Entrant() {LastName = "Лопаткин", YearOfAdmission = 2020, NumberOfSchool = 10});
entrants.Add(new Entrant() {LastName = "Новиков", YearOfAdmission = 2015, NumberOfSchool = 20});
entrants.Add(new Entrant() {LastName = "Веселовская", YearOfAdmission = 2014, NumberOfSchool = 10});
entrants.Add(new Entrant() {LastName = "Голованова", YearOfAdmission = 2019, NumberOfSchool = 20});
entrants.Add(new Entrant() {LastName = "Корчагина", YearOfAdmission = 2019, NumberOfSchool = 20});

Console.WriteLine("Список абитуриентов:");
foreach (var var in entrants)
{
    Console.WriteLine($"Фамилия: {var.LastName},№ школы:{var.NumberOfSchool}, год поступления: {var.YearOfAdmission}");
}
var entrant = entrants
    .OrderBy(i=>i.NumberOfSchool)
    .GroupBy(i => i.NumberOfSchool)
    .Select(i => new School()
    {
        NumberOfSchool = i.First().NumberOfSchool, QuantityEntrant = i.Count(), LastName = i.First().LastName
    });

Console.ForegroundColor = ConsoleColor.Red;
Console.WriteLine(@"Для каждой школы вывести общее число абитуриентов 
за все годы и фамилию первого из абитуриентов 
этой школы, содержащихся в исходном наборе данных 
(вначале указывать номер школы, затем число абитуриентов, затем фамилию). 
Сведения о каждой школе выводить на новой строке и упорядочивать 
по возрастанию номеров школ:");
Console.ForegroundColor = ConsoleColor.White;
foreach (var var in entrant)
{
    Console.WriteLine($"Номер школы: {var.NumberOfSchool}, всего абитуриентов: {var.QuantityEntrant}, фамилия первого ученика: {var.LastName}");
}

