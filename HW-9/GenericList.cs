﻿namespace HW_9;

//Реализовать generic класс GenericList<T> с методами Add(T item) и Print(). 
public class GenericList<T>
{
        //Внутри класса реализовать массив объектов (T[]),  
        private readonly T[] _array = new T[5];
        private int _i = 0;
        //в который можно добавить элемент через метод Add.
        public void Add(T item)
        {
                if (_i > _array.Length - 1)
                {
                        //Если вызвать метод Add больше раз, чем количество элементов в
                        //массиве - выводить предупреждение  в консоли.
                        Console.WriteLine("Предупреждение, колличество элементов в массиве меньше, чем вызываете! ");
                        _i = 0;
                }

                _array[_i] = item;
                _i++;
               

        }
        //Метод Print выводит в консоль все элементы. Количество элементов задается в конструкторе.
        public void Print()
        {
                foreach (var item in _array)
                {
                       
                        Console.WriteLine(item);
                }
        }
}
//Создать класс наследник GenericList<T>
public class MyClass<TG> : GenericList<TG>
{
        
}
//Реализовать класс Printer 
public class Printer
{
 

        //с generic методом Print<T>(T item), где T должен быть классом.
        public static void Print<T>(T item) where T : class
        {
              Console.WriteLine(item.ToString()); //Метод выводит в консоль строковое представление объекта. (obj.ToString)
        }
}
