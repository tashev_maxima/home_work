﻿// See https://aka.ms/new-console-template for more information
using HW_9;

var genericList = new GenericList<int>();
genericList.Add(10);
genericList.Add(9);
genericList.Add(11);
genericList.Add(12);
genericList.Add(13);
genericList.Add(14);
genericList.Print();

var myClass = new MyClass<string>();
myClass.Add("str");
myClass.Print();

//(obj.ToString);
Printer.Print(genericList);
Printer.Print(myClass);