﻿namespace HW_13_Extensions;

public static class Extensions 
{
    
    public static void ReplaceToZero(this int[] array)
    {
        var rand = new Random();
        /*реализовать в проекте методы расширения для массива целочисленных значений (int[])
заполнить отрицательные элементы массива нулями*/
        for (int i = 0; i < array.Length; i++)
        {
            array[i] = rand.Next(-10, 10);
            if (array[i]<0)
            {
                array[i] = 0;
            }
        }
        //Исходный массив
        Console.WriteLine("\nИсходный массив:");
        for (int i = 0; i < array.Length; i++)
        {
            Console.WriteLine(array[i]);
        }
        // Массив в обратном порядке
        Console.WriteLine("\nМассив в обратном порядке:");
        for (int i = array.Length; i > 0; i--)
        {
            Console.WriteLine(array[i-1]);
        }
       
    }
    /* Создать метод расширения для обобщенных массивов, который позволяет перемешать все элементы массива*/
    public static void Shuffle<T>(this T[] array)
    {
        var random = new Random();
        for (int i = array.Length - 1; i >= 1; i--)
        {
            int j = random.Next(i + 1);
            // обменять значения data[j] и data[i]
            var temp = array[j];
            array[j] = array[i];
            array[i] = temp;
        }
        Console.WriteLine("\nПеремешиваем обобщенный массив:")
                          ;
        for (int i = 0; i < array.Length; i++)
        {
            Console.WriteLine(array[i]);
        }
          
    }
    
}
