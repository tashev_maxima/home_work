﻿Console.WriteLine($"Готовим завтрак! Поток: {Thread.CurrentThread.ManagedThreadId}");


await PourCoffee();
await Task.WhenAll(
    FryEggs(),
    FryBacon(),
    ToastBread()
);
await JamOnBread();
await PourJuice();
Console.WriteLine($"Завтрак готов! Поток: {Thread.CurrentThread.ManagedThreadId}");

async Task PourCoffee()
{
    Console.WriteLine($"Наливаем кофе Поток: {Thread.CurrentThread.ManagedThreadId}");
    await Task.Delay(1000);
}

async Task HeatPan()
{
    Console.WriteLine($"Греем сковородку Поток: {Thread.CurrentThread.ManagedThreadId}");
    await Task.Delay(1000);
}

async Task FryEggs()
{
    Console.WriteLine($"Жарим яйца Поток: {Thread.CurrentThread.ManagedThreadId}");
    await Task.Delay(1000);
}

async Task FryBacon()
{
    Console.WriteLine($"Жарим бекон Поток: {Thread.CurrentThread.ManagedThreadId}");
    await Task.Delay(2000);
}

async Task ToastBread()
{
    Console.WriteLine($"Готовим тосты Поток: {Thread.CurrentThread.ManagedThreadId}");
    await Task.Delay(1000);
}

async Task JamOnBread()
{
    Console.WriteLine($"Мажем джем на тосты Поток: {Thread.CurrentThread.ManagedThreadId}");
    await Task.Delay(1000);
}

async Task<string> PourJuice()
{
    Console.WriteLine($"Наливаем сок Поток: {Thread.CurrentThread.ManagedThreadId}");
    await Task.Delay(1000);
    return "Rich";
}