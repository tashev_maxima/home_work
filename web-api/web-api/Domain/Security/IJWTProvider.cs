namespace Domain.Security;

public interface IJWTProvider
{
    string GetToken(string login, string role, int id);
}