using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Domain.Security;

public class JWTProvider : IJWTProvider
{
    private readonly DomainOptions _options;

    public JWTProvider(IOptions<DomainOptions> options)
    {
        _options = options.Value;
    }

    public string GetToken(string login, string role, int id)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        const string KEYVALUE = "super-puper-sekretKey!!!!!!-super-puper-sekretKey!!!!!!";
        var key = Encoding.ASCII.GetBytes(KEYVALUE);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, login),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, role),
                new Claim("userId", id.ToString())
            }),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature),
        
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var res = tokenHandler.WriteToken(token);
        return res;
    }
    
}
