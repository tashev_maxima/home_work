﻿using DataAccessLayer.Models;

namespace web_api.Models;

/// <summary>
/// Билет
/// </summary>
public class Ticket
{
    /// <summary>
    /// Индентификатор билета
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Назване фильма
    /// </summary>
    public string NameMovies { get; set; }

    /// <summary>
    /// Жанр фильма
    /// </summary>
    public string Genre { get; set; }

    /// <summary>
    /// Возрастное ограничение
    /// </summary>
    public int AgeRating { get; set; }

    /// <summary>
    /// Номер заказа
    /// </summary>
    public int OrderNumber { get; set; }

    /// <summary>
    /// Дата сеанса
    /// </summary>
    public DateTime SessionTime { get; set; }

    /// <summary>
    /// Цена
    /// </summary>
    public decimal Price { get; set; }

    /// <summary>
    /// Номер зала
    /// </summary>
    public int HallNumber { get; set; }

    /// <summary>
    /// Ряд
    /// </summary>
    public int Row { get; set; }

    /// <summary>
    /// Место
    /// </summary>
    public int Place { get; set; }

    /// <summary>
    /// Индификатор владельца билета
    /// </summary>
    public int UserDataId { get; set; } // внешний ключ

    /// <summary>
    /// Данные о владельце билета
    /// </summary>
    public ICollection<UserData>? UserDatas { get; set; }
}