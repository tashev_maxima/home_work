﻿using AutoMapper;
using DataAccessLayer.Models;
using web_api.Models;
namespace Domain.Tickets;
/// <summary>
/// Автомапер билетов
/// </summary>
public class TicketMapProfile : Profile
{
    public  TicketMapProfile ()
    {
        CreateMap<TicketData, Ticket>()
            .ForMember(data => data.Id, expression => expression.MapFrom(ticket => ticket.Id))
            .ForMember(data => data.NameMovies, expression => expression.MapFrom(ticket => ticket.Name))
            .ForMember(data => data.AgeRating, expression => expression.MapFrom(ticket => ticket.Age))
            .ForMember(data => data.OrderNumber, expression => expression.MapFrom(ticket => ticket.Order))
            .ForMember(data => data.SessionTime, expression => expression.MapFrom(ticket => ticket.Time))
            .ForMember(data => data.HallNumber, expression => expression.MapFrom(ticket => ticket.Hall))
            .ForMember(data => data.UserDataId, expression => expression.MapFrom(ticket => ticket.UserDataId))
            .ForMember(data => data.UserDatas, expression => expression.MapFrom(ticket => ticket.UserDatas));

        CreateMap<Ticket, TicketData>()
            .ForMember(ticket => ticket.Id, expression => expression.MapFrom(data => data.Id))
            .ForMember(ticket => ticket.Name, expression => expression.MapFrom(data => data.NameMovies))
            .ForMember(ticket => ticket.Age, expression => expression.MapFrom(data => data.AgeRating))
            .ForMember(ticket => ticket.Order, expression => expression.MapFrom(data => data.OrderNumber))
            .ForMember(ticket => ticket.Time, expression => expression.MapFrom(data => data.SessionTime))
            .ForMember(ticket => ticket.Hall, expression => expression.MapFrom(data => data.HallNumber))
            .ForMember(ticket => ticket.UserDataId, expression => expression.MapFrom(data => data.UserDataId))
            .ForMember(ticket => ticket.UserDatas, expression => expression.MapFrom(data => data.UserDatas));
        
    } 
    
}