using FluentValidation;

namespace Domain.Tickets.Queries;
/// <summary>
/// Проверка полей билета
/// </summary>
public class GetTicketQueryValidator : AbstractValidator<GetTicketQuery>
{
    public GetTicketQueryValidator()
    {
        RuleFor(query => query.Id).GreaterThanOrEqualTo(0);
    }
}