﻿using MediatR;
using web_api.Models;

namespace Domain.Tickets.Queries;
/// <summary>
/// Поля билета, которые будут ипользоватся для его получения
/// </summary>
public class GetTicketQuery : IRequest<Ticket>
{
    public int Id { get; set; }
}