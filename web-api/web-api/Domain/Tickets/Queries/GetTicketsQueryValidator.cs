using FluentValidation;

namespace Domain.Tickets.Queries;
/// <summary>
/// Проверка полей билетов
/// </summary>
public class GetTicketsQueryValidator : AbstractValidator<GetTicketsQuery>
{
    public GetTicketsQueryValidator()
    {
        RuleFor(query => query.Skip).GreaterThanOrEqualTo(0);
        RuleFor(query => query.Take).GreaterThanOrEqualTo(0);
        RuleFor(query => query.Skip).Must((query, skip) => skip <= query.Take);
    }
}