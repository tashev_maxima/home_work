﻿using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;
using web_api.Models;

namespace Domain.Tickets.Queries;
/// <summary>
/// Получение билетов по их полям
/// </summary>
public class GetTicketsQueryHandler : IRequestHandler<GetTicketsQuery, IEnumerable<Ticket>>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    public GetTicketsQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    public async Task<IEnumerable<Ticket>> Handle(GetTicketsQuery request, CancellationToken cancellationToken)
    {
        var tickets = await _dataContext.Tickets
            .Skip(request.Skip)
            .Take(request.Take)
            .AsNoTracking() // отключаем слежку, что бы запрос выполнялся быстрее
            .ToListAsync(cancellationToken: cancellationToken);
        
        return _mapper.Map<IEnumerable<Ticket>>(tickets);
    }
}