﻿using AutoMapper;
using DataAccessLayer;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using web_api.Models;

namespace Domain.Tickets.Queries;
/// <summary>
/// Получение билета по его полям
/// </summary>
public class GetTicketQueryHandler : IRequestHandler<GetTicketQuery, Ticket>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    public GetTicketQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    public async Task<Ticket> Handle(GetTicketQuery request, CancellationToken cancellationToken)
    {
        var ticket = await _dataContext.Tickets
            .FirstOrDefaultAsync(t => t.Id == request.Id,cancellationToken:cancellationToken);
        if (ticket == null)
            throw new NotFoundException($"Билет c id={request.Id} не найден");
        return _mapper.Map<Ticket>(ticket);
    }
}