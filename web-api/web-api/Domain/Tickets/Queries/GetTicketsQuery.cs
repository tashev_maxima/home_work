using MediatR;
using web_api.Models;

namespace Domain.Tickets.Queries;
/// <summary>
/// Поля билета, которые будут ипользоватся для получения нескольких штук
/// </summary>
public class GetTicketsQuery : IRequest<IEnumerable<Ticket>>
{
    public int Skip { get; set; }
    public int Take { get; set; }
}