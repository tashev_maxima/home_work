﻿using MediatR;

namespace Domain.Tickets.Commands;

/// <summary>
/// Поле билета по которое будит использоваться для удаление билета
/// </summary>
public class DeleteTicketCommand : IRequest<int>
{
    /// <summary>
    /// Индификатор билета
    /// </summary>
    public int Id { get; set; }
}