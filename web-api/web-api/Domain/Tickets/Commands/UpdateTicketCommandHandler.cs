﻿using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;


namespace Domain.Tickets.Commands;

/// <summary>
/// Обновление полей билета
/// </summary>
public class UpdateTicketCommandHandler : IRequestHandler<UpdateTicketCommand, int>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    public UpdateTicketCommandHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    public async Task<int> Handle(UpdateTicketCommand request, CancellationToken cancellationToken)
    {
        var ticket = await _dataContext.Tickets.FirstOrDefaultAsync(t => t.Id == request.Id);

        if (ticket == null)
        {
            return default;
        }

        ticket.Name = request.NameMovies;
        ticket.Genre = request.Genre;
        ticket.Age = request.AgeRating;
        ticket.Order = request.OrderNumber;
        ticket.Time = request.SessionTime;
        ticket.Price = request.Price;
        ticket.Hall = request.HallNumber;
        ticket.Row = request.Row;
        ticket.Place = request.Place;
        await _dataContext.SaveChangesAsync();
        return request.Id;
    }
}