﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using MediatR;

namespace Domain.Tickets.Commands;

/// <summary>
/// Создание билета
/// </summary>
public class CreateTicketCommandHandler : IRequestHandler<CreateTicketCommand, int>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    public CreateTicketCommandHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    public async Task<int> Handle(CreateTicketCommand request, CancellationToken cancellationToken)
    {
        var ticketData = _mapper.Map<TicketData>(request);
        await _dataContext.Tickets.AddAsync(ticketData);
        await _dataContext.SaveChangesAsync();
        return ticketData.Id;
    }
}