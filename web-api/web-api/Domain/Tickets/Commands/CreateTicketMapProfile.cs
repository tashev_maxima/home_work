﻿using AutoMapper;
using DataAccessLayer.Models;

namespace Domain.Tickets.Commands;

/// <summary>
/// Авотмапер билета
/// </summary>
public class CreateTicketMapProfile : Profile
{
    public CreateTicketMapProfile()
    {
        CreateMap<CreateTicketCommand, TicketData>()
            .ForMember(data => data.Id, expression => expression.MapFrom(command => command.Id))
            .ForMember(data => data.Name, expression => expression.MapFrom(command => command.NameMovies))
            .ForMember(data => data.Age, expression => expression.MapFrom(command => command.AgeRating))
            .ForMember(data => data.Order, expression => expression.MapFrom(command => command.OrderNumber))
            .ForMember(data => data.Time, expression => expression.MapFrom(command => command.SessionTime))
            .ForMember(data => data.Hall, expression => expression.MapFrom(command => command.HallNumber))
            .ForMember(data => data.UserDataId, expression => expression.MapFrom(command => command.UserDataId))
            .ForMember(data => data.UserDatas, expression => expression.MapFrom(command => command.UserDatas));

        CreateMap<TicketData, CreateTicketCommand>()
            .ForMember(command => command.Id, expression => expression.MapFrom(ticketDate => ticketDate.Id))
            .ForMember(command => command.NameMovies, expression => expression.MapFrom(ticketDate => ticketDate.Name))
            .ForMember(command => command.AgeRating, expression => expression.MapFrom(ticketDate => ticketDate.Age))
            .ForMember(command => command.OrderNumber, expression => expression.MapFrom(ticketDate => ticketDate.Order))
            .ForMember(command => command.SessionTime, expression => expression.MapFrom(ticketDate => ticketDate.Time))
            .ForMember(command => command.HallNumber, expression => expression.MapFrom(ticketDate => ticketDate.Hall))
            .ForMember(command => command.UserDataId, expression => expression.MapFrom(ticketDate => ticketDate.UserDataId))
            .ForMember(command => command.UserDatas, expression => expression.MapFrom(ticketDate => ticketDate.UserDatas));
    }
}