﻿using FluentValidation;

namespace Domain.Tickets.Commands;

/// <summary>
/// Проверка полей билета
/// </summary>
public class CreateTicketCommandValidator : AbstractValidator<CreateTicketCommand>
{
    public CreateTicketCommandValidator()
    {
        RuleFor(command => command.Id).NotNull().GreaterThanOrEqualTo(0);
        RuleFor(command => command.NameMovies).NotNull().NotEmpty().Length(3, 20);
        RuleFor(command => command.UserDataId).NotNull().GreaterThanOrEqualTo(0);
    }
}