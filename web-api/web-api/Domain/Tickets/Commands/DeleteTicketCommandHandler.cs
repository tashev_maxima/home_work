﻿using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Tickets.Commands;

/// <summary>
/// Удаление билета
/// </summary>
public class DeleteTicketCommandHandler : IRequestHandler<DeleteTicketCommand, int>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    public DeleteTicketCommandHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    public async Task<int> Handle(DeleteTicketCommand request, CancellationToken cancellationToken)
    {
        var ticket = await _dataContext.Tickets.FirstOrDefaultAsync(t => t.Id == request.Id);
        _dataContext.Tickets.Remove(ticket);
        await _dataContext.SaveChangesAsync();
        return request.Id;
    }
}