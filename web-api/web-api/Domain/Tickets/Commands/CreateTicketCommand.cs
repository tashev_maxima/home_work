﻿using DataAccessLayer.Models;
using MediatR;

namespace Domain.Tickets.Commands;

/// <summary>
/// Поля билета, которые будут ипользоватся для его создания
/// </summary>
public class CreateTicketCommand : IRequest<int>
{
    /// <summary>
    /// Индентификатор билета
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Назване фильма
    /// </summary>
    public string NameMovies { get; set; }

    /// <summary>
    /// Жанр фильма
    /// </summary>
    public string Genre { get; set; }

    /// <summary>
    /// Возрастное ограничение
    /// </summary>
    public int AgeRating { get; set; }

    /// <summary>
    /// Номер заказа
    /// </summary>
    public int OrderNumber { get; set; }

    /// <summary>
    /// Дата сеанса
    /// </summary>
    public DateTime SessionTime { get; set; }

    /// <summary>
    /// Цена
    /// </summary>
    public decimal Price { get; set; }

    /// <summary>
    /// Номер зала
    /// </summary>
    public int HallNumber { get; set; }

    /// <summary>
    /// Ряд
    /// </summary>
    public int Row { get; set; }

    /// <summary>
    /// Место
    /// </summary>
    public int Place { get; set; }

    /// <summary>
    /// Индификатор владельца билета
    /// </summary>
    public int UserDataId { get; set; }

    /// <summary>
    /// Данные о владельце билета
    /// </summary>
    public ICollection<UserData>? UserDatas { get; set; }
}