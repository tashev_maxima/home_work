﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using MediatR;

namespace Domain.Users.Queries;

public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, int>

{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    public CreateUserCommandHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    public async Task<int> Handle(CreateUserCommand request, CancellationToken cancellationToken)
    {
        var userData = _mapper.Map<UserData>(request);
        await _dataContext.Users.AddAsync(userData);
        await _dataContext.SaveChangesAsync();
        return userData.Id;
    }

    
}