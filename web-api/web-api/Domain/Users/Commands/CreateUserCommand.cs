﻿using DataAccessLayer.Models;
using MediatR;

namespace Domain.Users.Queries;

public class CreateUserCommand : IRequest<int>
{
    /// <summary>
    /// Индификатор пользователя
    /// </summary>
    public int Id { get; set; }
    
    public string Login { get; set; }
    public string Password { get; set; }
    /// <summary>
    /// Номер телефона пользователя
    /// </summary>
    public string? PhoneNumber { get; set; }

    /// <summary>
    /// Имя пользователя
    /// </summary>
    public string FirstName { get; set; }

    /// <summary>
    /// Фамилия пользователя
    /// </summary>
    public string LastName { get; set; }

    /// <summary>
    /// Количество полных лет пользователя
    /// </summary>
    public int Age { get; set; }

    /// <summary>
    /// Роль пользователя
    /// </summary>
    public RoleData Role { get; set; }

    /// <summary>
    /// Данные о билетах прикрепленные к пользователю
    /// </summary>
    public virtual ICollection<TicketData>? TicketDatas { get; set; }
}