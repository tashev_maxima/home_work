﻿using AutoMapper;
using DataAccessLayer.Models;
using Domain.Users.Queries;

namespace Domain.Users.Commands;

public class CreateUserMapProfile : Profile
{
    public CreateUserMapProfile()
    {
        CreateMap<CreateUserCommand, UserData>()
            .ForMember(user => user.Id, expression => expression.MapFrom(command => command.Id))
            .ForMember(user => user.Age, expression => expression.MapFrom(command => command.Age))
            .ForMember(user => user.FirstName, expression => expression.MapFrom(command => command.FirstName))
            .ForMember(user => user.LastName, expression => expression.MapFrom(command => command.LastName))
            .ForMember(user => user.PhoneNumber, expression => expression.MapFrom(command => command.PhoneNumber))
            .ForMember(user => user.Role, expression => expression.MapFrom(command => command.Role))
            .ForMember(user => user.Login, expression => expression.MapFrom(command => command.Login))
            .ForMember(user => user.Password, expression => expression.MapFrom(command => command.Password));
        
        CreateMap<UserData, CreateUserCommand>()
            .ForMember(command => command.Id, expression => expression.MapFrom(user => user.Id))
            .ForMember(command => command.Age, expression => expression.MapFrom(user => user.Age))
            .ForMember(command => command.FirstName, expression => expression.MapFrom(user => user.FirstName))
            .ForMember(command => command.LastName, expression => expression.MapFrom(user => user.LastName))
            .ForMember(command => command.PhoneNumber, expression => expression.MapFrom(user => user.PhoneNumber))
            .ForMember(command => command.Role, expression => expression.MapFrom(user => (int)user.Role))
            .ForMember(command => command.Password, expression => expression.MapFrom(user => user.Login))
            .ForMember(command => command.Password, expression => expression.MapFrom(user => user.Password));
    }
}