﻿using FluentValidation;

namespace Domain.Users.Queries;

public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
{
    public CreateUserCommandValidator()
    {
        RuleFor(command => command.Id).NotNull().GreaterThanOrEqualTo(0);
        RuleFor(command => command.Login).NotNull().NotEmpty().Length(3, 20);
        RuleFor(command => command.Password).NotNull();
    }
}