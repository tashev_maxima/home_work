﻿using AutoMapper;
using DataAccessLayer.Models;

namespace Domain.Users;

public class UserAutoMapConfig : Profile
{
    public UserAutoMapConfig()
    {
        CreateMap<UserData, User>()
            .ForMember(data => data.Id, expression => expression.MapFrom(user => user.Id))
            .ForMember(data => data.Age, expression => expression.MapFrom(user => user.Age))
            .ForMember(data => data.FirstName, expression => expression.MapFrom(user => user.FirstName))
            .ForMember(data => data.LastName, expression => expression.MapFrom(user => user.LastName))
            .ForMember(data => data.PhoneNumber, expression => expression.MapFrom(user => user.PhoneNumber))
            .ForMember(data => data.Role, expression => expression.MapFrom(user => (int) user.Role));

        CreateMap<User, UserData>()
            .ForMember(user => user.Id, expression => expression.MapFrom(data => data.Id))
            .ForMember(user => user.Age, expression => expression.MapFrom(data => data.Age))
            .ForMember(user => user.FirstName, expression => expression.MapFrom(data => data.FirstName))
            .ForMember(user => user.LastName, expression => expression.MapFrom(data => data.LastName))
            .ForMember(user => user.PhoneNumber, expression => expression.MapFrom(data => data.PhoneNumber))
            .ForMember(user => user.Role, expression => expression.MapFrom(data => data.Role));
        
    }
}