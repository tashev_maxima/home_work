﻿using System.Text.Json;
using Domain.Exceptions;

namespace web_api.Middlewares;

public class ExceptionCustomMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<ExceptionCustomMiddleware> _logger;

    public ExceptionCustomMiddleware(RequestDelegate next, ILoggerFactory? loggerFactory)
    {
        _next = next;
        _logger = loggerFactory?.CreateLogger<ExceptionCustomMiddleware>() ??
                  throw new ArgumentNullException(nameof(loggerFactory));
    }

    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            _logger.LogInformation("Request URL: {@Context}", context);
            await _next(context);
        }
        catch (NotFoundException notFoundException)
        {
            _logger.LogError(notFoundException.Message);
            context.Response.StatusCode = StatusCodes.Status404NotFound;
            context.Response.ContentType = "application/json";
            var response = JsonSerializer.Serialize(new { errors = notFoundException.Message });
            await context.Response.WriteAsync(response);
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            context.Response.StatusCode = StatusCodes.Status404NotFound;
            await context.Response.WriteAsync(e.Message);
        }
        
    }
}