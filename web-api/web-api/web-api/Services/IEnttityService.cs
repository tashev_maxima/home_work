namespace web_api.Services;

public interface IEnttityService<T>
{
    T? GetId(int id);
    IEnumerable<T> GetAll();
    void Create(T entity);
    void Remove(int id);
    void Update(T entity);
}