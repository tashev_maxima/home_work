﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using web_api.Models;


namespace web_api.Services;

public class TicketService : ITicketService
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    public TicketService(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    public IEnumerable<Ticket> GetAll()
    {
        _dataContext.ChangeTracker.QueryTrackingBehavior =
            QueryTrackingBehavior.NoTracking; // выкл отслежевание изменений
        var ticket = _dataContext.Tickets
            .Select(t => _mapper.Map<Ticket>(t))
            .ToList();
        return ticket;
    }

    public Ticket? GetId(int ticketId)
    {
        var ticket = _dataContext.Tickets
            .FirstOrDefault(t => t.Id == ticketId);
        _dataContext.Entry(ticket)
            .Reference(ticket => ticket.UserData)
            .Load();
        return ticket == null ? null : _mapper.Map<Ticket>(ticket);
    }


    public void Create(Ticket ticket)
    {
        var ticketData = _mapper.Map<TicketData>(ticket);
        _dataContext.Tickets.Add(ticketData);
        _dataContext.SaveChanges();
    }

    public void Update(Ticket ticket)
    {
        var ticketData = _dataContext.Tickets.FirstOrDefault(t => t.Id == ticket.Id);
        if (ticket == null) return;
        ticketData.Name = ticket.NameMovies;
        ticketData.Genre = ticket.Genre;
        ticketData.Age = ticket.AgeRating;
        ticketData.Order = ticket.OrderNumber;
        ticketData.Time = ticket.SessionTime;
        ticketData.Price = ticket.Price;
        ticketData.Hall = ticket.HallNumber;
        ticketData.Row = ticket.Row;
        ticketData.Place = ticket.Place;
        _dataContext.SaveChanges();
    }

    public void Remove(int ticketId)
    {
        var ticketData = _dataContext.Tickets.FirstOrDefault(t => t.Id == ticketId);

        if (ticketData == null) return;
        _dataContext.Tickets.Remove(ticketData);
        _dataContext.SaveChanges();

    }


}