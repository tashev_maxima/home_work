﻿namespace web_api.Services;

public interface IApiKeyProvider
{
    string GetKey();
}