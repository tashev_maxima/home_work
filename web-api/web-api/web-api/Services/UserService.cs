﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using Domain.Users;
using Microsoft.EntityFrameworkCore;
using web_api.Models;

namespace web_api.Services;

public class UserService : IUserService
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;
    public UserService(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;

    }
    
    public IEnumerable<User> GetAll()
    {
        var user = _dataContext.Users
            .AsNoTracking()
            .Include(user1 => user1.TicketDatas)
            .Select(usr =>_mapper.Map<User>(usr))
            .ToList();
        return user;
    }
    
    public User GetId(int userId)
    {
        var user = _dataContext.Users
            .AsNoTracking()
            .Include(u=>u.TicketDatas)
            .FirstOrDefault(u => u.Id == userId);
        return user == null ? null : _mapper.Map<User>(user);
    }
    
    public void Create(User user)
    {
        var userData = _mapper.Map<UserData>(user);
        _dataContext.Users.Add(userData);
        _dataContext.SaveChanges();
        
    }
    public void Update(User user)
    {
        var userData = _dataContext.Users.FirstOrDefault(usr =>usr.Id== user.Id);
        if (user == null) return;
        userData.Age = user.Age;
        userData.FirstName = user.FirstName;
        userData.LastName = user.LastName;
        userData.PhoneNumber = user.PhoneNumber;
        _dataContext.SaveChanges();
    }
    public void Remove(int userId)
    {
        var userData = _dataContext.Users.FirstOrDefault(u => u.Id == userId);

        if (userData == null) return;
        _dataContext.Users.Remove(userData);
        _dataContext.SaveChanges();
    }

    
}