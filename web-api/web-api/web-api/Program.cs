using System.Security.Claims;
using System.Text;
using System.Text.Json.Serialization;
using DataAccessLayer;
using Domain.Security;
using Domain.Tickets;
using Domain.Tickets.Commands;
using Domain.Tickets.Queries;
using Domain.Users;
using Domain.Users.Commands;
using FluentValidation;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using web_api;
using web_api.Extensions;
using web_api.Middlewares;
using web_api.Models;
using web_api.Requirements;
using web_api.Services;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<DataContext>(optionsBuilder =>
    optionsBuilder.UseNpgsql("Host=localhost;Port=5433;Database=HomeWorkDB;Username=postgres;Password=270788"));
builder.Services.AddControllers();
builder.Services.AddControllersWithViews()
    .AddJsonOptions(x => x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    {
        options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
        {
            In = ParameterLocation.Header,
            Description = "Please insert JWT with Bearer into field",
            Name = "Authorization",
            Type = SecuritySchemeType.ApiKey,
            BearerFormat = "JWT"
        });
        
        options.AddSecurityRequirement(new OpenApiSecurityRequirement()
        {
            {
                new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference {Type = ReferenceType.SecurityScheme, Id = "Bearer"}
                },
                new string[] { }
            }
        });
        
        options.CustomSchemaIds(y => y.FullName);
    }
    {
        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "web-api.xml"));
    }
    {
        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Domain.xml"));
    }
});
builder.Services.AddTransient<IUserService,UserService>();
builder.Services.AddTransient<ITicketService,TicketService>();
builder.Services.AddTransient<IApiKeyProvider, ApiKeyProvider>();
builder.Services.AddTransient<IJWTProvider, JWTProvider>();
builder.Services.AddTransient<ApiKeyMiddleware>();
builder.Services.AddTransient<AbstractValidator<User>, UserValidator>();
builder.Services.AddFluentValidationAutoValidation();
builder.Services.AddValidatorsFromAssemblyContaining<UserValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<CreateTicketCommandValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<GetTicketQueryValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<GetTicketsQueryValidator>();
builder.Services.AddAutoMapper(typeof(UserAutoMapConfig),typeof(TicketMapProfile), typeof(CreateTicketMapProfile),typeof(CreateUserMapProfile));
builder.Services.AddMediatR(typeof(CreateTicketCommand));
builder.Services.AddScoped<IAuthorizationHandler, MinimalAgeRequirementHandler>();
builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    options.RequireAuthenticatedSignIn = false;
}).AddJwtBearer(options =>
{
    options.RequireHttpsMetadata = false;
    options.SaveToken = true;
    options.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("super-puper-sekretKey!!!!!!-super-puper-sekretKey!!!!!!")),
        ValidateIssuer = false,
        ValidateAudience = false
    };
});
builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("OnlyAdminOrUser",
        policyBuilder => policyBuilder.RequireClaim(ClaimsIdentity.DefaultRoleClaimType, "Admin", "User"));
    options.AddPolicy(Policies.ONLYADULTPOLICY,policyBuilder => policyBuilder.AddRequirements(new MinimalAgeRequirement(18)));
});

var app = builder.Build();



app.UseExceptionCustomMiddlewares();
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseReDoc(options => options.RoutePrefix = "redoc");
}

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();