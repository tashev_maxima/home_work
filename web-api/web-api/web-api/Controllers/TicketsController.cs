﻿using Domain.Tickets.Commands;
using Domain.Tickets.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using web_api.Models;
using web_api.Services;

namespace web_api.Controllers;

/// <summary>
/// CRUID для пользователя 
/// </summary>
[Authorize]
[ApiController]
[Route("api/[controller]")]
public class TicketsController : ControllerBase
{
    private readonly ITicketService _ticketService;
    private readonly IMediator _mediator;

    public TicketsController(ITicketService ticketService, IMediator mediator)
    {
        _ticketService = ticketService;
        _mediator = mediator;
    }
    
    /// <summary>
    /// Получение всех билетов
    /// </summary>
    /// <returns> Все билеты </returns>
    /// <response code="401">Неверный логин или пароль</response>
    /// <response code="403">У вас недостаточно прав для совершения данной операции</response>
    [Authorize(Policy = Policies.ONLYADULTPOLICY)]
    [HttpGet("ALL")]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult GetAll()
    {
        var ticket = _ticketService.GetAll();
        return Ok(ticket);
    }
    
    /// <summary>
    /// Получение билетов через метод Skip/Take
    /// </summary>
    /// <param name="getTicketsQuery"> пагинация </param>
    /// <returns> Результирующий набор билетов </returns>
    /// <response code="401">Неверный логин или пароль </response>
    /// <response code="403">У вас недостаточно прав для совершения данной операции</response>
    [Authorize(Policy = "OnlyAdminOrUser")]
    [HttpGet(("SKIP & TAKE"))]
    [Produces("application/json")]
    [ProducesResponseType(typeof(IEnumerable<Ticket>), StatusCodes.Status200OK, "application/json")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult<IEnumerable<Ticket>>> Get([FromQuery] GetTicketsQuery getTicketsQuery)
    {
        var tickets = await _mediator.Send(getTicketsQuery);
        return Ok(tickets);
    }
    /// <summary>
    /// Получение билета по идентификатору
    /// </summary>
    /// <param name="id"> Идентификатор билета</param>
    /// <returns> Билет </returns>
    /// <response code="401">Неверный логин или пароль</response>
    /// <response code="403">У вас недостаточно прав для совершения данной операции</response>
    /// <response code="404">Билет с таким идентификатором не найден</response>
    [Authorize(Policy = "OnlyAdminOrUser")]
    [HttpGet("{id}")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(Ticket), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Get(int id)
    {
        var getTicketQuery = new GetTicketQuery { Id = id };
        var ticket = await _mediator.Send(getTicketQuery);
        return Ok(ticket);
    }
    /// <summary>
    /// Добавление билета пользователю
    /// </summary>
    /// <response code="401">Неверный логин или пароль</response>
    /// <response code="403">У вас недостаточно прав для совершения данной операции</response>
    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Create([FromQuery] CreateTicketCommand createTicketCommand)
    {
        await _mediator.Send(createTicketCommand);
        return Ok();
    }
    /// <summary>
    /// Изменение информации в билете
    /// </summary>
    /// <response code="401">Неверный логин или пароль</response>
    /// <response code="403">У вас недостаточно прав для совершения данной операции</response>
    [Authorize(Roles = "Admin")]
    [HttpPut]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Update(int id, [FromQuery] UpdateTicketCommand updateTicketCommand)
    {
        await _mediator.Send(updateTicketCommand);
        updateTicketCommand.Id = id;
        return Ok();
    }
    
    /// <summary>
    /// Удаление билете по индификатуру
    /// </summary>
    /// <response code="401">Неверный логин или пароль</response>
    /// <response code="403">У вас недостаточно прав для совершения данной операции</response>
    /// <response code="404">Билет с таким идентификатором не найден</response>
    [Authorize(Roles = "Admin")]
    [HttpDelete]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> Delete(int id)
    {
        await _mediator.Send(new DeleteTicketCommand{ Id = id });
        return Ok();
    }
}