﻿using System.Threading.Tasks;
using DataAccessLayer.Models;
using Domain.Users;
using Domain.Users.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using web_api.Models;
using web_api.Services;

namespace web_api.Controllers;

/// <summary>
/// CRUID для пользователя 
/// </summary>
[Authorize]
[ApiController]
[Route("api/[controller]")]
public class UserController : ControllerBase
{
    private readonly IUserService _userService;
    private readonly IMediator _mediator;

    
    public UserController(IUserService userService, IMediator mediator)
    {
        _userService = userService;
        _mediator = mediator;
    }

    /// <summary>
    /// Получение всех пользователей
    /// </summary>
    /// <returns> Все пользователи</returns>
    /// <response code="401">Неверный логин или пароль</response>
    /// <response code="403">У вас недостаточно прав для совершения данной операции</response>
    [Authorize(Roles = "Admin,User,Guest")]
    [HttpGet("ALL")]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult Get()
    {
        var user = _userService.GetAll();
        return Ok(user);
    }

    /// <summary>
    /// Получение информации о пользователе по идентификатору
    /// </summary>
    /// <param name="id"> Идентификатор пользователя</param>
    /// <returns> Информация о пользователе </returns>
    /// <response code="401">Неверный логин или пароль</response>
    /// <response code="403">У вас недостаточно прав для совершения данной операции</response>
    /// <response code="404">Пользователь с таким идентификатором не найден</response>
    [Authorize(Policy = "OnlyAdminOrUser")]
    [HttpGet("{id}")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(Ticket), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult Get(int id)
    {
        var user = _userService.GetId(id);
        if (user != null)
        {
            return Ok(user);
        }

        return NotFound();
    }

    /// <summary>
    /// Добавление пользователя
    /// </summary>
    /// <response code="401">Неверный логин или пароль</response>
    /// <response code="403">У вас недостаточно прав для совершения данной операции</response>
    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Create([FromQuery] CreateUserCommand createUserCommand)
    {
        await _mediator.Send(createUserCommand);
        return Ok();
    }

    /// <summary>
    /// Изменение элементов сведений о пользователе
    /// </summary>
    /// <response code="401">Неверный логин или пароль</response>
    /// <response code="403">У вас недостаточно прав для совершения данной операции</response>
    /// <response code="404">Пользователь с таким идентификатором не найден</response>
    [Authorize(Roles = "Admin")]
    [HttpPut("{id}")]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult Update(int id, [FromQuery] User userData)
    {
        var user = _userService.GetId(id);
        if (user == null)
            return NotFound();

        userData.Id = id;
        _userService.Update(userData);
        return Ok();
    }

    /// <summary>
    /// Удаление пользователя по индификатуру
    /// </summary>
    /// <response code="401">Неверный логин или пароль</response>
    /// <response code="403">У вас недостаточно прав для совершения данной операции</response>
    /// <response code="404">Пользователь с таким идентификатором не найден</response>
    [Authorize(Policy = Policies.ONLYADULTPOLICY)]
    [HttpDelete]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult Delete(int id)
    {
        var user = _userService.GetId(id);
        if (user == null) return NotFound();
        _userService.Remove(id);
        return Ok();

    }
}