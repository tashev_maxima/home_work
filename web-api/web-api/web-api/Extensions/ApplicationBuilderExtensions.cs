﻿using web_api.Middlewares;

namespace web_api.Extensions;

public static class ApplicationBuilderExtensions
{
    public static void UseExceptionCustomMiddlewares(this IApplicationBuilder applicationBuilder)
    {
        applicationBuilder.UseMiddleware<ExceptionCustomMiddleware>();
    }

    public static void UseApiKeyMiddleware(this IApplicationBuilder applicationBuilder)
    {
        applicationBuilder.UseMiddleware<ApiKeyMiddleware>();
    }
}