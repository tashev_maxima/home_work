﻿namespace web_api;

public class ApiResult
{
    public bool Success { get; set; }
    public int StatusCode { get; set; }
    public string ErrorsMessage { get; set; }
}

public class ApiResult<T> : ApiResult
{
    public T Date { set; get; }
}