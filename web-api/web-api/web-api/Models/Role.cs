﻿namespace web_api.Models;

/// <summary>
/// Роли пользователя
/// </summary>
public enum Role
{
    /// <summary>
    /// Без роли
    /// </summary>
    Disable = -1,

    /// <summary>
    /// Гость
    /// </summary>
    Guest = 1,

    /// <summary>
    /// Поьзователь
    /// </summary>
    User = 2,

    /// <summary>
    /// Администратор
    /// </summary>
    Admin = 777
}