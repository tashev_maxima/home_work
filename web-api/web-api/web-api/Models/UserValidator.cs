﻿using Domain.Users;
using FluentValidation;

namespace web_api.Models;

public class UserValidator : AbstractValidator<User>
{
    public UserValidator()
    {
        RuleFor(user => user.FirstName).NotNull().NotEmpty().MinimumLength(1).MaximumLength(15).WithMessage("Длина должна быть от 1 до 15");
        RuleFor(user => user.LastName).NotNull().NotEmpty().MinimumLength(1).MaximumLength(25).WithMessage("Длина должна быть от 1 до 25");
        RuleFor(user => user.Age).NotNull().NotEmpty().LessThan(150).WithMessage("Не более 150");
        RuleFor(user => user.Role).NotEmpty();
    }
}