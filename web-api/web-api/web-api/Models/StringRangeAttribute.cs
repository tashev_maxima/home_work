﻿using System.ComponentModel.DataAnnotations;

namespace web_api.Models;

/// <summary>
/// Атрибут длины строки
/// </summary>
public class StringLengthAttribute : ValidationAttribute
{
    private readonly int _from;
    private readonly int _to;

    public StringLengthAttribute(int from, int to)
    {
        _from = from;
        _to = to;
    }

    /// <summary>
    /// Проверка длины строки
    /// </summary>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        var ticket = (Ticket)validationContext.ObjectInstance;
        if (ticket.NameMovies.Length < _from || ticket.NameMovies.Length > _to)
        {
            return new ValidationResult($"Длина должна быть от {_from} до {_to}");
        }

        return ValidationResult.Success;
    }
}