﻿using DataAccessLayer.Migrations;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations;

public class UserConfiguration: IEntityTypeConfiguration<UserData>
{
    public void Configure(EntityTypeBuilder<UserData> modelBuilder)
    {
        modelBuilder.HasKey(user => user.Id);
        modelBuilder.Property("FirstName").IsRequired().HasMaxLength(20);
        modelBuilder.Property("LastName").IsRequired().HasMaxLength(20);
        modelBuilder.Property("Login").IsRequired().HasMaxLength(20);
        modelBuilder.Property("Password").IsRequired().HasMaxLength(20);
    }
}  