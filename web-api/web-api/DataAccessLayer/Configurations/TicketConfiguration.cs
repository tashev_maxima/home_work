﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations;

public class TicketConfiguration: IEntityTypeConfiguration<TicketData>
{
    public void Configure(EntityTypeBuilder<TicketData> modelBuilder)
    {
        modelBuilder.HasKey(ticket => ticket.Id);
        modelBuilder
            .HasOne<UserData>(ticket => ticket.UserData) //один ко многим
            .WithMany(user => user.TicketDatas) // у одного пользователя может быть много билетов
            .OnDelete(DeleteBehavior.Cascade);
        modelBuilder.ToTable("Tickets");
        modelBuilder.Property("Name").HasMaxLength(100);
    }
}