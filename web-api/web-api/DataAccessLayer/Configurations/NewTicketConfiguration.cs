﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations;

public class NewTicketConfiguration : IEntityTypeConfiguration<NewTicket>
{
    public void Configure(EntityTypeBuilder<NewTicket> modelBuilder)
    {
        modelBuilder.ToTable("NewTickets");
    }
}
