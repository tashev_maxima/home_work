﻿namespace DataAccessLayer.Models;

/// <summary>
/// Билет
/// </summary>
public class TicketData
{
    /// <summary>
    /// Индентификатор билета
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Назване фильма
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Жанр
    /// </summary>
    public string Genre { get; set; }

    /// <summary>
    /// Возрастное ограничение
    /// </summary>
    public int Age { get; set; }

    /// <summary>
    /// Номер заказа
    /// </summary>
    public int Order { get; set; }

    /// <summary>
    /// Дата сеанса
    /// </summary>
    public DateTime Time { get; set; }

    /// <summary>
    /// Цена
    /// </summary>
    public decimal Price { get; set; }

    /// <summary>
    /// Номер зала
    /// </summary>
    public int Hall { get; set; }

    /// <summary>
    /// Ряд
    /// </summary>
    public int Row { get; set; }

    /// <summary>
    /// Место
    /// </summary>
    public int Place { get; set; }

    /// <summary>
    /// Индификатор владельца билета
    /// </summary>
    public int UserDataId { get; set; } // внешний ключ

    /// <summary>
    /// Данные о владельце билета
    /// </summary>

    public ICollection<UserData>? UserDatas { get; set; }

    public virtual UserData UserData { get; set; } //навигационное свойство
}