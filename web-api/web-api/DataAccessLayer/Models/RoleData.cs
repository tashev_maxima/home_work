﻿namespace DataAccessLayer.Models;

public enum RoleData
{
    Disable = -1,
    Guest = 1,
    User = 2,
    Admin = 777
}