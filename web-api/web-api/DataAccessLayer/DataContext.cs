﻿using DataAccessLayer.Configurations;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer;

public sealed class DataContext : DbContext
{
    public DbSet<UserData>? Users { get; set; }
    public DbSet<TicketData>? Tickets { get; set; }
    public DbSet<NewTicket>? NewTickets { get; set; }
    

    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {
       Database.Migrate();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(UserConfiguration).Assembly);
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(TicketConfiguration).Assembly);
    }
}