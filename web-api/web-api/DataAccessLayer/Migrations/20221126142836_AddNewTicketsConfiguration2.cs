﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class AddNewTicketsConfiguration2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "MovieTickets");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "MovieTickets");

            migrationBuilder.CreateTable(
                name: "NewTickets",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewTickets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NewTickets_MovieTickets_Id",
                        column: x => x.Id,
                        principalTable: "MovieTickets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NewTickets");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "MovieTickets",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "MovieTickets",
                type: "text",
                nullable: false,
                defaultValue: "");
        }
    }
}
