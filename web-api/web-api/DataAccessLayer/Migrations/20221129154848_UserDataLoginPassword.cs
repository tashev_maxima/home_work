﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class UserDataLoginPassword : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MovieTickets_Users_UserDataId",
                table: "MovieTickets");

            migrationBuilder.DropForeignKey(
                name: "FK_NewTickets_MovieTickets_Id",
                table: "NewTickets");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_MovieTickets_TicketDataId",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Users",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MovieTickets",
                table: "MovieTickets");

            migrationBuilder.DropIndex(
                name: "IX_MovieTickets_UserDataId",
                table: "MovieTickets");

            migrationBuilder.RenameTable(
                name: "MovieTickets",
                newName: "Tickets");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "Users",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer")
                .OldAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddColumn<string>(
                name: "Login",
                table: "Users",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "Users",
                type: "character varying(20)",
                maxLength: 20,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "UserDataLogin",
                table: "Tickets",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Users",
                table: "Users",
                column: "Login");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tickets",
                table: "Tickets",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_UserDataLogin",
                table: "Tickets",
                column: "UserDataLogin");

            migrationBuilder.AddForeignKey(
                name: "FK_NewTickets_Tickets_Id",
                table: "NewTickets",
                column: "Id",
                principalTable: "Tickets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tickets_Users_UserDataLogin",
                table: "Tickets",
                column: "UserDataLogin",
                principalTable: "Users",
                principalColumn: "Login",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Tickets_TicketDataId",
                table: "Users",
                column: "TicketDataId",
                principalTable: "Tickets",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NewTickets_Tickets_Id",
                table: "NewTickets");

            migrationBuilder.DropForeignKey(
                name: "FK_Tickets_Users_UserDataLogin",
                table: "Tickets");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Tickets_TicketDataId",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Users",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tickets",
                table: "Tickets");

            migrationBuilder.DropIndex(
                name: "IX_Tickets_UserDataLogin",
                table: "Tickets");

            migrationBuilder.DropColumn(
                name: "Login",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Password",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserDataLogin",
                table: "Tickets");

            migrationBuilder.RenameTable(
                name: "Tickets",
                newName: "MovieTickets");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "Users",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer")
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Users",
                table: "Users",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MovieTickets",
                table: "MovieTickets",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_MovieTickets_UserDataId",
                table: "MovieTickets",
                column: "UserDataId");

            migrationBuilder.AddForeignKey(
                name: "FK_MovieTickets_Users_UserDataId",
                table: "MovieTickets",
                column: "UserDataId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NewTickets_MovieTickets_Id",
                table: "NewTickets",
                column: "Id",
                principalTable: "MovieTickets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_MovieTickets_TicketDataId",
                table: "Users",
                column: "TicketDataId",
                principalTable: "MovieTickets",
                principalColumn: "Id");
        }
    }
}
