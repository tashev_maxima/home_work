﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DataAccessLayer;

public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
{
    public DataContext CreateDbContext(string[] args)
    {
        var optionsBuilder = new DbContextOptionsBuilder<DataContext>()
            .UseNpgsql("Host=localhost;Port=5433;Database=HomeWorkDB;Username=postgres;Password=270788");
        return new DataContext(optionsBuilder.Options);
    }
}