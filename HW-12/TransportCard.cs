﻿namespace HW_12;

public class TransportCard
{
    private int _balance;
    
    public event MessageHandle? OnMessageHandle;

    

    public TransportCard()
    {
        _balance = 300;
    }

    public void ReplenishmentOftheBalance()
    {
        /*var rand = new Random();
        _balance += rand.Next(1,30);*/
        _balance += 25;
        
        
        OnMessageHandle?.Invoke($"(Карта пополнена на 25 руб. Ваш баланс:{_balance.ToString()})");
        
    }
    
    public void Payment()
    {
        
        
        _balance -= 30;
        OnMessageHandle?.Invoke($"Оплата проезда - 30 руб. Ваш баланс:{_balance.ToString()})");
        if (_balance < 30 && _balance> 0)
        {
            if (OnMessageHandle != null)
            {
                OnMessageHandle.Invoke($"Недостаточно денег для оплаты проезда, ваш баланс составляет {_balance} руб.");
            }
        }
        
    }

   
}

public delegate void MessageHandle(string message);