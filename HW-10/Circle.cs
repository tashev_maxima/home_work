﻿namespace HW_10;

public class Circle : Figure
{
    private float radius;
   
    
    
    
    public Circle(float radius, string figureName) : base(figureName)
    {
        this.radius = radius;
       
    }

    public override double GetSquare()
    {
        return  radius * radius * Math.PI; // S=πR²
    }
}