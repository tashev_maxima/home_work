﻿namespace HW_10;

//Создать абстрактный класс Figure, 
public abstract class Figure
{
    //который содержит  приватное поле name (название фигуры);
    private string name = "noname";
    //конструктор с 1 параметром, инициализирующий поле name указанным значением;
    public Figure(string s)
    {
        
        figureName = s;
    }

  

    //публичное свойство Name для доступа к приватному полю name;
    public string figureName
    {
        get
        {
            return name;
        }
        set
        {
            name = value;
        }
    }
    // абстрактный метод GetSquare(), предназначенный для получения площади фигуры;
    public abstract double GetSquare();
    // виртуальный метод Print(), который выводит название фигуры
    public virtual string Print()
    {
        return $"Площадь {figureName}:{GetSquare()}";
    }
}
