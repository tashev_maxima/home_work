﻿namespace HW_10;

public  class Rectangle : Figure
{
    private double a;
    private double b;
    public Rectangle(double a, double b, string figureName) : base(figureName)
    {
        this.a = a;
        this.b = b;
    }
    
    public override double GetSquare() => a * b;
    

}