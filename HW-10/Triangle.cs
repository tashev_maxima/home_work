﻿namespace HW_10;

public class Triangle : Figure
{
    private double _side;
    private double _h;//высота триугольника
    
    
    
    public Triangle(double side,double h, string figureName) : base(figureName)
    {
        _side = side;
        _h = h;
    }

    public override double GetSquare()// S = 1/2a * h
    {
        return (float)(0.5 * _side * _h);
    }
}