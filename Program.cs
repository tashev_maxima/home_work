﻿
/*int[] myArray = new int[] { 1, 2, 3, 4, 5 };
int sum = 0;
void SumArray(int[] myArr)
{
    for (int i = 0; i < myArr.Length; i++)
    {

        sum += +myArr[i];

    }

    Console.WriteLine("Сумма массива " + sum);
}


SumArray(myArray);*/



//Реализовать метод для подсчета суммы чисел в целочисленном массиве
int[] myArray = { 1, 2, 3, 4, 5 };
int sum = myArray.Sum();
Console.WriteLine("Сумма массива: {0}", sum);
//Реализовать метод для вывода массива в консоль
//вертикально/горизонтально (в зависимости от параметра метода)
void Print(bool vertical)
{
    int[] myArray = { 1, 2, 3, 4, 5 };
    if (vertical)
    {
        for (int i = 0; i < myArray.Length; i++)
        {
            Console.Write($"\t{myArray[i]}");
        }
    }
    else
    {
        for (int i = 0; i < myArray.Length; i++)
        {
            Console.Write($"\n{myArray[i]}");
        }
    }
        
}
void PrintValues()
{
    Console.WriteLine("Нажмите 1 если по вертикали, иначе по шоризонтале");
    var horisont = Console.ReadLine();
    if (horisont=="1")
    {
        Print(false);
    }
    else
    {
        Print(true);
    }
}
PrintValues();

Console.ReadKey();