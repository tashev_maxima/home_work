﻿namespace HW_7;

public class Human
{
    private string _name;
    private int _age;
    private int _height;
    private int _weight;
    private EyeColor _eyeColor;


    public int AgeProperty
    {
        get
        {
            return _age;
        }

        set
        {
            if (value >= 0 && value < 121)
                _age = value;
            
        }
        
    }

    /// <summary>
    ///  Рост в сантиметрах
    /// </summary>
    public int HeightProperty
    {
        get
        {
            return _height;
        }
        set
        {
            if (value < 301)  
            {
                _height = value;
            }
        }
    }
    public int WeightProperty
    {
        get
        {
            return _weight;
        }
        set
        {
            if (value > 0)
            {
                _weight = value;
            }
        }
    }
    
    
    public void Print()
    {
        Console.WriteLine($"Имя: {_name}\nВозраст: {_age}\nРост: {_height}\nВес: {_weight}\nЦвет глаз: {_eyeColor}\n");
    }
    public Human(string name, EyeColor color)
    {
        _name = name;
        _eyeColor = color;
    }

   
}