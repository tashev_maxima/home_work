﻿
/*Реализовать класс, описывающий человека и его следующие характеристики:
Имя (строка)
Возраст (положительное число не более 120)
Рост (положительное число не более 3м)
Вес (положительное число)
Цвет глаз (enum)
В классе должны быть:
private поля для хранения данных
public свойства для получения и изменения данных (при изменении проверять условия, указанные выше)
public метод экземпляра класса для печати информации о человеке в консоль*/

using HW_7;

Human humanVasya = new Human("Vasya",EyeColor.Red)
{
    AgeProperty = 30,
    HeightProperty = 175,
    WeightProperty = 70
};

Human humanKatya = new Human("Katya",EyeColor.Green)
{
    AgeProperty = 32,
    HeightProperty = 165,
    WeightProperty = 50
};

humanVasya.Print();
humanKatya.Print();