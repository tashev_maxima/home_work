﻿/*● Дана целочисленная последовательность. Извлечь из нее все нечетные числа, сохранив их исходный порядок следования и удалив
 все вхождения повторяющихся элементов, кроме первых.

● Дана последовательность положительных целых чисел. Обрабатывая только нечетные числа, получить последовательность
 их строковых представлений (.ToString) и отсортировать ее в лексикографическом порядке по возрастанию (по алфавиту).

● Исходная последовательность содержит сведения о клиентах фитнес-центра. Каждый элемент последовательности включает следующие целочисленные поля:

<Код клиента> <Год> <Номер месяца> <Продолжительность занятий (в часах)>

Найти элемент последовательности с минимальной продолжительностью занятий. Вывести эту продолжительность, а также соответствующие
 ей год и номер месяца (в указанном порядке на той же строке). Если имеется несколько элементов с минимальной продолжительностью, 
 то вывести данные того из них, который является последним в исходной последовательности.*/

using HW_16;

Console.ForegroundColor = ConsoleColor.Yellow; 
Console.WriteLine("++++++++++++++<<The first task>>++++++++++++++");
Console.ForegroundColor = ConsoleColor.Red;

var numbers = new List<int>();

for (int i = 0; i < 10; i++)
{
   var rndNum = new Random().Next(-10, 10);
   numbers.Add(rndNum);
}

Console.WriteLine("Исходный список случайных чисел:");
Console.ForegroundColor = ConsoleColor.White;

foreach (var num in numbers)
{
   Console.Write($"{num}\t");
}


var task1 = numbers.Where(numbers => numbers % 2 != 0).Distinct(); //Решение       

Console.ForegroundColor = ConsoleColor.Red;
Console.WriteLine("\nИзвлекаем нечетные числа.Удаляем все повторяющиеся элементы, кроме первых:");
Console.ForegroundColor = ConsoleColor.White;

foreach (var t in task1)
{
 Console.Write($"{t}\t");
}

Console.ForegroundColor = ConsoleColor.Yellow; 
Console.WriteLine("\n\n++++++++++++++<<The second task>>++++++++++++++");
Console.ForegroundColor = ConsoleColor.Blue;
Console.WriteLine(@"Дана последовательность положительных целых чисел.
Обрабатывая только нечетные числа, получить последовательность их строковых представлений (.ToString)
и отсортировать ее в лексикографическом порядке по возрастанию (по алфавиту)");
var task2=numbers
                                     .Where(numbers=>numbers>0 && numbers%2!=0)/*Взял числа из первого задания, поэтому множественный выбор */
                                     .Select(numbers=>numbers.ToString())
                                     .OrderBy(numbers=>numbers);

Console.ForegroundColor = ConsoleColor.Red;
Console.WriteLine("\nПовторный исходный список случайных чисел, что бы проверить ответ:");
Console.ForegroundColor = ConsoleColor.White;

foreach (var num in numbers)
{
 Console.Write($"{num}\t");
}

Console.ForegroundColor = ConsoleColor.Red;
Console.WriteLine("\nОтвет: ");
foreach (var t2 in task2)
{
 Console.Write($"{t2}\t");
}

Console.ForegroundColor = ConsoleColor.Yellow; 
Console.WriteLine("\n\n++++++++++++++<<The third task>>++++++++++++++");

Console.ForegroundColor = ConsoleColor.White;

List<Clients> clients = new List<Clients>();


clients.Add(new Clients(){ClientCode = 0006,Year = 2009,MonthNumber = 7,DurationOfClassesInHours = 36});
clients.Add(new Clients(){ClientCode = 0302,Year = 2011,MonthNumber = 3,DurationOfClassesInHours = 28});
clients.Add(new Clients(){ClientCode = 1802,Year = 2015,MonthNumber = 12,DurationOfClassesInHours = 56});
clients.Add(new Clients(){ClientCode = 1967,Year = 2015,MonthNumber = 4,DurationOfClassesInHours = 28});
clients.Add(new Clients(){ClientCode = 2178,Year = 2016,MonthNumber = 7,DurationOfClassesInHours = 38});

foreach (var client in clients)
{
 Console.WriteLine($"Номер:{client.ClientCode}, {client.Year}г., {client.MonthNumber}мес., {client.DurationOfClassesInHours}час.");
}
Console.ForegroundColor = ConsoleColor.DarkRed;
IEnumerable<Clients> clientsEnumerable = clients;
var task3 = clients
 .Where(client => client.DurationOfClassesInHours == 
                  clients.MinBy(client => client.DurationOfClassesInHours)!.DurationOfClassesInHours)
 .Select(client =>
  $"Ответ: \nМеньше всего занимались {client.DurationOfClassesInHours} часов в {client.Year} году {client.MonthNumber} месяца."
 ).Last();


Console.Write(task3);

