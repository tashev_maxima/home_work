﻿namespace HW_16;
/*<Код клиента> <Год> <Номер месяца> <Продолжительность занятий (в часах)> */
public class Clients
{
    public int ClientCode { get; set; }
    public int Year { get; set; }
    public int MonthNumber { get; set; }
    public int DurationOfClassesInHours { get; set; }
}