﻿namespace HW_11;
// Создать интерфейс IFigure, который содержит
public interface IFigure
{
    // метод GetSquare(), предназначенный для получения площади фигуры
    public double GetSquare();
}