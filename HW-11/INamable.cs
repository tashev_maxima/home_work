﻿namespace HW_11;

// Создать интерфейс INamable, который содержит:
public interface INamable
{
    // свойство Name, предназначенное для получения названия
    string Name { get; }
}