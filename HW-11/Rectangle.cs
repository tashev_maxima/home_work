﻿namespace HW_11;
// Создать класс Rectangle, который реализует IFigure и INamable и содержит:
public class Rectangle : IFigure,INamable
{
    // приватные внутренние поля a, b (стороны прямоугольника) и name;
    private double a, b;
    private string name;
   
    // конструктор с 3 параметрами (название, стороны прямоугольника);
    public Rectangle(double a, double b, string name)
    {
        this.a = a;
        this.b = b;
        this.name = name;
    }
    
    // свойство Name, которое возвращает значение поля name
    public string Name => name;
    
    // метод GetSquare(), возвращающий площадь прямоугольника по его сторонам;
    public double GetSquare()
    {
        return a * b;
    }
}